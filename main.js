var slideIndex; //global variable for portfolio slide

// /menu link
$(document).ready(function(){
		var count = 0;
    $(".menu-link").click(function(){
    	count++;
    	if(count%2 !== 0){
        	$("#menu").css({"left":"0", "transition":"left 600ms ease 0s"});
        	$(".span1").css({"transform":"rotate(45deg)", "top":"11px",});
        	$(".span2").css("opacity","0");
        	$(".span3").css({"transform":"rotate(-45deg)", "top":"11px",});
    	}else {
        	$("#menu").css({"left":"-100%", "transition":"left 600ms ease 0s"});
        	$(".span1").css({"transform":"rotate(0deg)", "top":"0",});
        	$(".span2").css("opacity","1");
        	$(".span3").css({"transform":"rotate(0deg)", "top":"30px",});
    	}	
    });
    $("#menu").click(function(){
    	count++;
    	if(count%2 === 0) {
			$("#menu").css({"left":"-100%", "transition":"left 600ms ease 0s"});
	    	$(".span1").css({"transform":"rotate(0deg)", "top":"0"});
	    	$(".span2").css("opacity","1");
	    	$(".span3").css({"transform":"rotate(0deg)", "top":"30px"});
    	}
    });
});

// home - typewritter
$(document).ready(function(){
	var text = document.getElementById("text");
	var str = ["Hi there, Iam  James Smith.",
	     "I'm  UI/UX designer.",
	     "I have strong knowledge in web development",
	    "As well as in photoshop, css3",
	    "Cross browser compiled layout"], i = 0, j=0;

	    setInterval(function () {
	  		datatxt = str[j];
	    	len = datatxt.length + 1;
	    	if(text !== null){
	    		text.innerHTML += datatxt[i++];
	    	}
			if (i == len){
	   			text.innerHTML = ""; i = 0; j++;
			}  
	    	
	    	if(j == str.length){
	      		text.innerHTML = datatxt;
	    	}
	    
	    }, 100);
});

//skills - chart
$(document).ready(function() {  
	$("#bars .barr1").css("height","225px");
	$("#bars .barr2").css("height","150px");
	$("#bars .barr3").css("height","215px");
	$("#bars .barr4").css("height","230px");
	$("#bars .barr5").css("height","128px");  
});

// portfolio gallery
$(document).ready(function(){
	
	$(".all").click(function() {
		$("#id-0, #id-1, #id-2, #id-3, #id-4, #id-5, #id-6, #id-7").show();
		
		$(".cat-item-1, .cat-item-2, .cat-item-3, .cat-item-4, .cat-item-5").removeClass("active");
		$(this).addClass("active");

		$(".class-0, .class-1, class-2, .class-3, .class-4, .class-5, .class-6, .class-7").addClass("mySlides");
	});
	$(".cat-item-1").click(function() {
		$("#id-6 , #id-2").show();
		$("#id-0, #id-1, #id-3, #id-4, #id-5, #id-7").hide();
		
		$(".all, .cat-item-2, .cat-item-3, .cat-item-4, .cat-item-5").removeClass("active");
		$(this).addClass("active");
		
		$(".class-0, .class-1, .class-3, .class-4, .class-5, .class-7").removeClass("mySlides");
		$(".class-2, .class-6").addClass("mySlides");
	});
	$(".cat-item-2").click(function() {
		$("#id-1 , #id-2").show();
		$("#id-0, #id-3, #id-4, #id-5, #id-6, #id-7").hide();

		$(".all, .cat-item-1, .cat-item-3, .cat-item-4, .cat-item-5").removeClass("active");
		$(this).addClass("active");
		
		$(".class-0, .class-3, .class-4, .class-5, .class-6, .class-7").removeClass("mySlides");
		$(".class-1, .class-2").addClass("mySlides");
	});
	$(".cat-item-3").click(function() {
		$("#id-4").show();
		$("#id-0, #id-1, #id-2, #id-3, #id-5, #id-6, #id-7").hide();

		$(".all, .cat-item-1, .cat-item-2, .cat-item-4, .cat-item-5").removeClass("active");
		$(this).addClass("active");
		
		$(".class-0, .class-1, .class-2, .class-3, .class-5, .class-6, .class-7").removeClass("mySlides");
		$(".class-4").addClass("mySlides");
	});
	$(".cat-item-4").click(function() {
		$("#id-0, #id-3").show();
		$("#id-1 , #id-2, #id-4, #id-5, #id-6, #id-7").hide();

		$(".all, .cat-item-1, .cat-item-2, .cat-item-3, .cat-item-5").removeClass("active");
		$(this).addClass("active");
		
		$(".class-1, .class-2, .class-4, .class-5, .class-6, .class-7").removeClass("mySlides");
		$(".class-0, .class-3").addClass("mySlides");
	});
	$(".cat-item-5").click(function() {
		$("#id-7").show();
		$("#id-0, #id-1 , #id-2, #id-3, #id-4, #id-5, #id-6").hide();

		$(".all, .cat-item-1, .cat-item-2, .cat-item-3, .cat-item-4").removeClass("active");
		$(this).addClass("active");
		
		$(".class-0, .class-1, .class-2, .class-3, .class-4, .class-5, .class-6").removeClass("mySlides");
		$(".class-7").addClass("mySlides");
	});
	
});



$(document).ready(function() {
	//portfolio img click
	$("#id-0").click(function() {
		$(".class-0").css("display","block");
		$(".class-1, .class-2, .class-3, .class-4, .class-5, .class-6, .class-7").css("display","none");
		slideIndex = 1;
	});
	$("#id-1").click(function() {
		$(".class-1").css("display","block");
		$(".class-0, .class-2, .class-3, .class-4, .class-5, .class-6, .class-7").css("display","none");
		slideIndex = 2;
	});
	$("#id-2").click(function() {
		$(".class-2").css("display","block");
		$(".class-0, .class-1, .class-3, .class-4, .class-5, .class-6, .class-7").css("display","none");
		slideIndex = 3;
	});
	$("#id-3").click(function() {
		$(".class-3").css("display","block");
		$(".class-0, .class-1, .class-2, .class-4, .class-5, .class-6, .class-7").css("display","none");
		slideIndex = 4;
	});
	$("#id-4").click(function() {
		$(".class-4").css("display","block");
		$(".class-0, .class-1, .class-2, .class-3, .class-5, .class-6, .class-7").css("display","none");
		slideIndex = 5;
	});
	$("#id-5").click(function() {
		$(".class-5").css("display","block");
		$(".class-0, .class-1, .class-2, .class-3, .class-4, .class-6, .class-7").css("display","none");
		slideIndex = 6;
	});
	$("#id-6").click(function() {
		$(".class-6").css("display","block");
		$(".class-0, .class-1, .class-2, .class-3, .class-4, .class-5, .class-7").css("display","none");	
		slideIndex = 7;
	});
	$("#id-7").click(function() {
		$(".class-7").css("display","block");
		$(".class-0, .class-1, .class-2, .class-3, .class-4, .class-5, .class-6").css("display","none");
		slideIndex = 8;	
	});

	//portfolo open-close slide
	$(".portfolio-item2").click(function() {
		$(".w3-contentw3-slide-container").css("display","block");
		$(".slideBack").css("display","block");
	});
	$(".slideBack").click(function() {
		$(".w3-contentw3-slide-container").css("display","none");
		$(".slideBack").css("display","none");
	});
});

function plusDivs(n) {
    showDivs(slideIndex += n);
}

function showDivs(n) {
    var x = document.getElementsByClassName("mySlides");
    if (n > x.length) {slideIndex = 1}
    if (n < 1) {slideIndex = x.length};
    for (var i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    x[slideIndex-1].style.display = "block";
}	

// contact - range value
function updateTextInput(val) {
    document.getElementById("budget-text").innerHTML = val; 
}